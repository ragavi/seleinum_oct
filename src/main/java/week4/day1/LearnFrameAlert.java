package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrameAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//To enter frame
		driver.switchTo().frame(0);
		//Click button
		driver.findElementByXPath("//button[text()='Try it']").click();
		//Switch to alert
		System.out.println(driver.switchTo().alert().getText());
		//Send values in textbox in alert
		driver.switchTo().alert().sendKeys("Ragavi");
		//Click/Accept alert OK button
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		System.out.println("Text you have enter in Alert: "+text);
		if (text.contains("Ragavi")) {
			System.out.println("Text contain name Ragavi");
		} else {
			System.out.println("Text doesnt contain name Ragavi");
		}
	}

}
