package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandle {
	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElementByLinkText("Contact Us").click();
	Set<String> windowHandles = driver.getWindowHandles();
	List<String> listWindow=new ArrayList<>();
	listWindow.addAll(windowHandles);
	driver.switchTo().window(listWindow.get(1));
	System.out.println(driver.getTitle());
	driver.quit();
	}
}
