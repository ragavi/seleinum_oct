package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		WebElement eleUserName=driver.findElementByXPath("//input[@id='username']");
		eleUserName.sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		WebElement eleLogin=driver.findElementByXPath("//input[@class='decorativeSubmit']");
		eleLogin.click();
		WebElement CrmLink=driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]");
		CrmLink.click(); 
		driver.findElementByXPath("//a[text()='Leads']").click();
		WebElement merge = driver.findElementByXPath("//a[text()='Merge Leads']");
		merge.click();
		//Click on From Lead icon
		driver.findElementByXPath("//img[@alt='Lookup']").click();
		//Switch to new window
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listWindow=new ArrayList<>();
		listWindow.addAll(windowHandles);
		driver.switchTo().window(listWindow.get(1));
		System.out.println(driver.getTitle());
		//Enter Lead ID
		driver.findElementByName("id").sendKeys("10515");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Click First Resulting lead
		driver.findElementByXPath("//a[text()='Ragavi']").click();
		//Switch to primary window
		String mainWindowHandle = driver.getWindowHandles().iterator().next();
		driver.switchTo().window(mainWindowHandle);
		System.out.println(driver.getTitle());
		//Click On ToLead icon
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		//Move to new window
		Set<String> windowHandles1 = driver.getWindowHandles();
		List<String> listWindow1=new ArrayList<>();
		listWindow1.addAll(windowHandles1);
		driver.switchTo().window(listWindow1.get(1));
		System.out.println(driver.getTitle());
		//Enter Lead ID
		driver.findElementByName("id").sendKeys("10518");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Click First Resulting lead
		driver.findElementByXPath("//a[text()='Nandi']").click();
		//Switch back to primary window
		String mainWindowHandle1 = driver.getWindowHandles().iterator().next();
		driver.switchTo().window(mainWindowHandle1);
		//Click Merge
		driver.findElementByLinkText("Merge").click();
		//Switch to alert then accept
		 driver.switchTo().alert().accept();
		//Click Find Lead
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10515");
		WebDriverWait wait=new WebDriverWait(driver, 40);
		WebElement findLead = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Find Leads']")));
		findLead.click();		
		//Error Msg	
	/*	String errMsg=driver.findElementByXPath("//div[text()='No records to display']").getText();
		String ExpectedMsg="No records to display";
		if(errMsg.equals(ExpectedMsg)) {
			System.out.println("Testcase Passed: No records to display");
		}else {
			System.out.println("Testcase Failed");
		}*/
		String text = driver.findElementByXPath("//div[@class='x-toolbar x-small-editor']/div[@class='x-paging-info']").getText();
		System.out.println("Result is :"+text);
	}

}
