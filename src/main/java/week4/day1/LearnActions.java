package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//To press Esc Key from Keyboard
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement wb=driver.findElementByXPath("//span[text()='Electronics']");
		Actions builder=new Actions(driver);
		WebElement apple = driver.findElementByLinkText("Apple");
		builder.moveToElement(wb).pause(2000).perform();
		builder.click(apple);
		
		
		

	}

}
