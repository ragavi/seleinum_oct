package week1.day1;

import java.util.Scanner;

public class Prime {

	public static void main(String[] args) {
		// TO find the given number is prime or not
		int num, i = 2;
		Scanner scan=new Scanner(System.in);
		num=scan.nextInt();
        while(i<=num/2)
        {
            if(num%i==0)
            {
               break;
            }
            ++i;
        }

        if (num==i) {
            System.out.println(num + " is a prime number.");
        }
        else {
            System.out.println(num + " is not a prime number.");
        }
        scan.close();
	}

}
