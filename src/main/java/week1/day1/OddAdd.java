package week1.day1;

import java.util.Scanner;

public class OddAdd {

	public static void main(String[] args) {
		// To add odd numbers from the given input
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the digit");
		int input=scan.nextInt();
		int sum=0;		
		while(input>0) {
			int num=input%10;
			if(num%2!=0) {
				sum=sum+num;
			}
			input=input/10;
		}
		System.out.println(sum);
		scan.close();
	}
	
}
