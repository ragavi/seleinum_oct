package week1.day1;

public class MobilePhone {
	public String getBrand() {
		return "Redmi";
	}
	public int getPrice() {
		return 14999;
	}
	public String getModel(String model) {
		if(model.equals("A2")) {
			return "Redmi A2";
		}
		else if(model.equals("Pro")) {
			return "Redmi pro 5";
		}
		else {
		return "No Model defined";
		}
	}
	/*private int getPwd() {
		return 12345;
	}
	private void getPwd() {
		
	}*/
	public boolean isSmartPhone() {
		return true;
	}
	/*public BigDecimal getTax() {
		return 505;
	}*/
}
