package week1.day1;

import java.util.Scanner;

public class Vote {

	public static void main(String[] args) {
		// If age greater than 18 Eligible for Vote else not eligible
		int age;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your Age:");
		age=scan.nextInt();
		if(age>18) {
			System.out.println("You are Eligible for voting");
		}
		else {
			System.out.println("You are not Eligible for voting");
		}
		scan.close();

	}

}
