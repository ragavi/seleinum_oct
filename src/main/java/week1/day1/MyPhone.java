package week1.day1;

public class MyPhone {

	public static void main(String[] args) {
		MobilePhone phone= new MobilePhone();
		String brand = phone.getBrand();
		System.out.println(brand);
		boolean smartPhone = phone.isSmartPhone();
		System.out.println(smartPhone);
		String model=phone.getModel("A2");
		System.out.println(model);
		int price = phone.getPrice();
		System.out.println(price);

	}

}
