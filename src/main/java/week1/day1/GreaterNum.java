package week1.day1;

import java.util.Scanner;

public class GreaterNum {

	public static void main(String[] args) {
		// Find the greatest number from the given input
		int num1,num2,num3;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the first number:");
		num1=scan.nextInt();
		System.out.println("Enter the second number:");
		num2=scan.nextInt();
		System.out.println("Enter the third number:");
		num3=scan.nextInt();
		if((num1>num2)&&(num1>num3)) {
			System.out.println("Greatest digit amoung three numbers: " +num1);
		}
		else if((num2>num1)&&(num2>num3)) {
			System.out.println("Greatest digit amoung three numbers: " +num2);
		}
		else if((num3>num1)&&(num3>num2)) {
			System.out.println("Greatest digit amoung three numbers: " +num3);
		}
		else {
			System.out.println("Given number is not distinct");
		}
		scan.close();
		
	}

}
