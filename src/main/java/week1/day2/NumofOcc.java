package week1.day2;

import java.util.Scanner;

public class NumofOcc {
	public static void main(String args[]) {
		String name;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the name:");
		name=scan.nextLine();		
		int count=0;
		for(int i=0;i<name.length();i++) {
		//System.out.println(name.charAt(i));
			if(name.charAt(i)=='A') {				
				count++;
			}
		}
		System.out.println("The char 'A' contains in the name: ");
		System.out.println(count);
		scan.close();
	}
}
