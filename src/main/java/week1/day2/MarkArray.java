package week1.day2;

import java.util.Scanner;

public class MarkArray {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the total number of students: ");
		int totalNumofStudents=scan.nextInt();
		int[] studentMark=new int[totalNumofStudents];
		for(int i=0;i<totalNumofStudents;i++) {
			System.out.println("Enter the students mark:" +(i+1));
			studentMark[i]=scan.nextInt();
		}
		int count = 0;
		for(int eachStudentMark : studentMark) {
			System.out.println("Entered mark for each student:");
			System.out.println(eachStudentMark);
			if(eachStudentMark>50) {
				count++;

			}			
		}
		System.out.println("Number of students get passed:");
		System.out.println(count);
		scan.close();
	}
}
