package week1.day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexEmail {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String email;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter email id:");
		email=scan.nextLine();
		//String pat="^[\\w\\.-_]+@[\\w\\.-]+\\.[a-z]{2,3}$";
		String pat="^[\\w\\.-_]+@[\\w\\.-]+\\.[a-z]{2,3}$";
		Pattern p=Pattern.compile(pat);
		Matcher m=p.matcher(email);
		System.out.println(m.matches());
		if(m.matches()==true) {
			System.out.println("Entered Email id is Valid");
		}
		else {
			System.out.println("Entered Email id is Invalid");
		}
		scan.close();
	}
}
