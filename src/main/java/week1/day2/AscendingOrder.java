package week1.day2;

import java.util.Scanner;

public class AscendingOrder {

	public static void main(String[] args) {
		// Ascending number from the list of array values
		int temp=0;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the size of an array:");
		int size=scan.nextInt();
		int listOfNum[]=new int[size];
		for(int i=0;i<listOfNum.length;i++) {
			listOfNum[i]=scan.nextInt();			
		}
		for(int i=0;i<listOfNum.length;i++) {
			for(int j=i+1;j<listOfNum.length;j++) {
				if(listOfNum[i]>listOfNum[j]) {
					temp=listOfNum[i];
					listOfNum[i]=listOfNum[j];
					listOfNum[j]=temp;
				}
			}
		}
		for(int i=0;i<listOfNum.length;i++) {
			System.out.println(listOfNum[i]);
		}
		scan.close();
	}

}
