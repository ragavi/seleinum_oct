package util;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReport {
@Test
public void report() throws IOException {
	//To Create a empty html template
	ExtentHtmlReporter htmlObj=new ExtentHtmlReporter("./reports/results.html");
	//To append the testcases
	htmlObj.setAppendExisting(true);
	//To Pass the testcase
	ExtentReports extent=new ExtentReports();
	//Attach each and individual report
	extent.attachReporter(htmlObj);
	//Description of the testcase
	ExtentTest test=extent.createTest("CreateLead","Create a Lead");
	//Author Name
	test.assignAuthor("RAGAVI");
	//Test Category
	test.assignCategory("Smoke Test");
	//Passed Test
	test.pass("TC1 Passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
	test.pass("TC2 Passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img2.png").build());
	test.pass("TC3 Passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img3.png").build());
	test.pass("TC4 Passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img4.png").build());
	//Failed Test
	test.fail("TC1 Failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
	test.fail("TC2 Failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img2.png").build());
	test.fail("TC3 Failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img3.png").build());
	test.fail("TC4 Failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img4.png").build());
	//Warning Message
	test.warning("Warning",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img10.png").build());
	//save 
	extent.flush();
}
}
