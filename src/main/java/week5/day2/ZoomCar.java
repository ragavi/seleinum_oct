package week5.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import wdmethods.SeMethods;

public class ZoomCar extends SeMethods{

	public void Zoom() throws InterruptedException {
		startApp("chrome","https://www.zoomcar.com/chennai");
		WebElement clkJourney = locateElement("linktext", "Start your wonderful journey");
		click(clkJourney);
		WebElement clkJourneyPlace = locateElement("xpath", "(//div[@class='items'])[2]");
		click(clkJourneyPlace);
		WebElement clkNext = locateElement("xpath", "//button[text()='Next']");
		click(clkNext);
		WebElement DateActual=locateElement("xpath", "(//div[@class='day'])[1]");
		String actual=getText(DateActual);
		click(DateActual);
		System.out.println(actual);
		WebElement clkNext1 = locateElement("xpath", "//button[text()='Next']");
		click(clkNext1);
		Thread.sleep(2000);
		WebElement DateCheck=locateElement("xpath", "//div[@class='day picked ']");
		String expected=getText(DateCheck);
		System.out.println(expected);
		verifyPartialText(DateActual, expected);
		WebElement clkDone=locateElement("xpath", "//button[text()='Done']");
		click(clkDone);
		List<WebElement> list = driver.findElementsByXPath("//div[@class='car-item']");
		System.out.println(list.size());
		Thread.sleep(2000);
		List<WebElement> listPrice = driver.findElementsByXPath("//div[@class='price']");	
		List<Integer> PriceAmount=new ArrayList<Integer>();
		for (WebElement allValues : listPrice) {
			String price=allValues.getText().replaceAll("\\D", "");
			PriceAmount.add(Integer.parseInt(price));
		}
		System.out.println(PriceAmount);
		Collections.sort(PriceAmount);
		Integer maxprice = Collections.max(PriceAmount);
		System.out.println("Max Price amount is : "+maxprice);
		WebElement brandName = driver.findElementByXPath("(//div[contains(text(),'"+maxprice+"')]/../../preceding-sibling::div)[2]/h3");
		System.out.println("Brand Name:" +brandName.getText());
		/*System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
		 //click journey
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[2]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		WebElement DateActual=driver.findElementByXPath("(//div[@class='day'])[1]");
		String actual=DateActual.getText();
		DateActual.click();
		System.out.println(actual);
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(2000);
		WebElement DateCheck = driver.findElementByXPath("//div[@class='day picked ']");
		String expected=DateCheck.getText();
		System.out.println(expected);
		//Thread.sleep(2000);
		if(actual.contains(expected)) {
			System.out.println("Both date's are same");
		}else {
			System.out.println("Date mismatched");
		}
		driver.findElementByXPath("//button[text()='Done']").click();
		List<WebElement> list = driver.findElementsByXPath("//div[@class='car-item']");
		System.out.println(list.size());
		Thread.sleep(2000);
		List<WebElement> listPrice = driver.findElementsByXPath("//div[@class='price']");	
		List<Integer> PriceAmount=new ArrayList<Integer>();
		for (WebElement allValues : listPrice) {
			String price=allValues.getText().replaceAll("\\D", "");
			PriceAmount.add(Integer.parseInt(price));
		}
		System.out.println(PriceAmount);
		Collections.sort(PriceAmount);
		Integer maxprice = Collections.max(PriceAmount);
		System.out.println("Max Price amount is : "+maxprice);
		WebElement brandName = driver.findElementByXPath("(//div[contains(text(),'"+maxprice+"')]/../../preceding-sibling::div)[2]/h3");
		System.out.println("Brand Name:" +brandName.getText());
		//driver.findElementByXPath("//button[text()='BOOK NOW']").click();
		//driver.close();*/
		
	}

}//div[@class='car-item']
