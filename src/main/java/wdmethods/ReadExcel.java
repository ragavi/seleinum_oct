package wdmethods;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel extends ProjectMethods{
	public static String[][] getExcelData(String excelFileName) throws IOException {
	XSSFWorkbook myBook=new XSSFWorkbook("./data/"+excelFileName+".xlsx");
	XSSFSheet sheet=myBook.getSheet("createLead");
	int rowCount=sheet.getLastRowNum();
	System.out.println("Row Count"+rowCount);
	//To print the physical count of the rows
	int physiNumOfRows=sheet.getPhysicalNumberOfRows();
	System.out.println("PhysicalRow count:"+physiNumOfRows);
	int cellCount=sheet.getRow(0).getLastCellNum();
	System.out.println("Last Count"+cellCount);
	String[][] data=new String[rowCount][cellCount];
	System.out.println("Cell Values are:");
	for(int i=1;i<=rowCount;i++) {
		XSSFRow row=sheet.getRow(i);
		for(int j=0;j<cellCount;j++) {
			XSSFCell cell=row.getCell(j);
			String cellValues=cell.getStringCellValue();
			data[i-1][j]=cellValues;
			System.out.println(cellValues);
		}
	}
	return data;
	
	}
}
