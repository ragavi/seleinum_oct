package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import util.Reporter;

public class SeMethods extends Reporter implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions op=new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			}else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The browser "+browser+" launched successfully");
			reportSteps("pass", "The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			reportSteps("fail", "The browser "+browser+" failed to launch");
		} 
		takeSnap();
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "tagname": return driver.findElementByTagName(locValue);
			case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
			case "cssSelectror": return driver.findElementByCssSelector(locValue);
			}
			reportSteps("pass", "The element located sucesfully by the locator "+locator);
		} catch (NoSuchElementException e) {
			//System.out.println("The element is not found");
			reportSteps("fail", "The element is not found");
		} catch (WebDriverException e) {
			//System.out.println("Unknown Exception occured");
			reportSteps("fail", "Unknown Exception occured");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
	    
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			//System.out.println("The data "+data+" enter successfully");
			reportSteps("pass", "The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			//System.out.println("The data "+data+" not enter successfully");
		   reportSteps("fail", "The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}
	
	@Override
	public void typeKeyBoard(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data,Keys.ENTER);
			//System.out.println("The data "+data+" enter successfully");
			reportSteps("pass", "The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			//System.out.println("The data "+data+" not enter successfully");
		   reportSteps("fail", "The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}
	
	@Override
	public void type_append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportSteps("pass", "The data "+data+" enter successfully");
			//System.out.println("The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			 reportSteps("fail", "The data "+data+" not enter successfully");
			//System.out.println("The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}

	public void click_withoutSnap(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The element "+ele+" clicked");
			reportSteps("pass", "The element "+ele+" clicked successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			 reportSteps("fail", "The element " +ele+ " is not clicked");
		} 
	}
	
	public void click(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The element "+ele+" clicked");
			reportSteps("pass", "The element "+ele+" clicked successfully");
		} catch (WebDriverException e) {
			e.printStackTrace();
			 reportSteps("fail", "The element " +ele+ " is not clicked");
		} 
		takeSnap();
	}
	
	@Override
	public String getText(WebElement ele) {
		String text="";
		try {
			text = ele.getText();
			reportSteps("pass", "The text " +text+ " is fetched");
			//System.out.println("The text " +text+ " is fetched");
			
		} catch (WebDriverException e) {
			e.printStackTrace();
			reportSteps("fail", "The text " +text+ " is fetched");
		}
		takeSnap();
		return text;
	
	}

	/*	@Override
		public void selectDropDownUsingText(WebElement ele, String value,String sel) {
		try {
			Select select1=new Select(ele);
			select1.selectByVisibleText(value);
		} catch (WebDriverException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select selectIndex=new Select(ele);
			selectIndex.selectByIndex(index);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Index out of range");
		}
	}*/
	@Override
	public void selectDropDownUsingText(WebElement ele, String value,String sel) {
		try {
			Select select1=new Select(ele);
			if(sel.equalsIgnoreCase("visible")) {
				select1.selectByVisibleText(value);
			}else if(sel.equalsIgnoreCase("value")) {
				select1.selectByValue(value);
			}else if(sel.equalsIgnoreCase("index")) {
				select1.selectByIndex(Integer.parseInt(value));
			}
			reportSteps("pass", "Selected WebElement" +ele+ "selectedBy" +sel+ " Value "+value);
			//System.out.println("Selected WebElement" +ele+ "selectedBy" +sel+ " Value "+value);
		} catch (NumberFormatException e) {
			//e.printStackTrace();
			reportSteps("fail", "Selected WebElement" +ele+ "selectedBy" +sel+ " Value "+value);
		}
		takeSnap();
	}
	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn= false;
		try {
		
			String title = driver.getTitle();
				if (title.equals(expectedTitle)) {
					//System.out.println("Browser Title is matched with expected Title");
					reportSteps("pass", "Browser Title " +title+ " is matched with expected Title " +expectedTitle);
					bReturn=true;
				} else {
					//System.out.println("Title mismatched");
					reportSteps("pass", "Browser Title " +title+ " is mismatched with expected Title " +expectedTitle);
				}
		} catch (Exception e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception while fetching title of the browser");
		}
		takeSnap();
			return bReturn;
			
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.equals(expectedText)) {
				//System.out.println("The text " +text+ " is exactly same as expecetd text " +expectedText);
				reportSteps("pass", "The text " +text+ " is exactly same as expecetd text " +expectedText);
			}else {
				//System.out.println("Text mismatched");
				reportSteps("pass", "The text " +text+ " is mismatched with the expecetd text " +expectedText);
			}
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception in verifiying the Exact Text of an element");
		}
		takeSnap();

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.contains(expectedText)) {
				//System.out.println("Partial Text match found");
				reportSteps("pass", "The text " +text+ " is Partially same as expecetd text " +expectedText);
			}else {
				//System.out.println("Partial Text match not present");
				reportSteps("pass", "The text " +text+ " is not Partially same as expecetd text " +expectedText);
			}
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception in verifiying the Partial Text of an element");
		}
		takeSnap();

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String attribute1 = ele.getAttribute(attribute);
			if(attribute1.equals(value)) {
				//System.out.println("Attribute value with Actual Value found");
				reportSteps("pass", "Attribute value " +attribute1+ " Exactly matched with Actual Value " +value);
			}else {
				//System.out.println("Attribute value with Actual Value not found");
				reportSteps("pass", "Attribute value " +attribute1+ " Exactly not matched with Actual Value " +value);
			}
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception in verifiying the Exact Attribute of an element");
		}
		takeSnap();

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attribute1 = ele.getAttribute(attribute);
			if(attribute1.contains(value)) {
				//System.out.println("Attribute value with Actual Value found");
				reportSteps("pass", "Attribute value " +attribute1+ " Partially matched with Actual Value " +value);
			}else {
				//System.out.println("Attribute value with Actual Value not found");
				reportSteps("pass", "Attribute value " +attribute1+ " Partially not matched with Actual Value " +value);
			}
		} catch (WebDriverException e) {
			reportSteps("fail", "Unknown Exception in verifiying the Partially Attribute of an element");
		}
		takeSnap();
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected==true) {
				//System.out.println("Verified the element" +ele+ "is selected");

				reportSteps("pass", "Verified the element" +ele+ "is selected");
			}else {
				//System.out.println("The element" +ele+ "is not seleceted");
				reportSteps("pass", "The element " +ele+ " is not seleceted");
			}
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception in verifiying selected element");
		}
		takeSnap();

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if(displayed==true) {
				//System.out.println("Verified the element" +ele+ "is displayed");
				reportSteps("pass", "Verified the element" +ele+ "is dipalyed");
			}else {
				//System.out.println("The element" +ele+ "is not displayed");
				reportSteps("pass", "Verified the element" +ele+ "is not displayed");
			}
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown Exception in verifiying displayed element");
		}
		takeSnap();

	}
	public void ParentWindow() {
		try {
			String mainWindowHandle = driver.getWindowHandles().iterator().next();
			driver.switchTo().window(mainWindowHandle);
			reportSteps("pass", "Parent Window Captured");
		} catch (NoSuchWindowException e) {
			//e.printStackTrace();
			reportSteps("fail", "Failed to Capture Parent Window");
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindow = driver.getWindowHandles();
			List<String> list=new ArrayList<>();
			list.addAll(allWindow);
			driver.switchTo().window(list.get(index));
			reportSteps("pass", "Window switched successfully");			
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("fail", "Unknown exception");
		}
		takeSnap();
	}
	

	@Override
	public void switchToFrame(WebElement ele,String fra,String value) {
		try {
			if(fra.equals("index")) {
				driver.switchTo().frame(Integer.parseInt(value));
				reportSteps("pass", "Switched to the frame successfully using " +fra);	
			}else if(fra.equals("nameOrId")) {
				driver.switchTo().frame(value);
				reportSteps("pass", "Switched to the frame successfully using " +fra);	
			}else if(fra.equals("WebElement")) {
				driver.switchTo().frame(value);
				reportSteps("pass", "Switched to the frame successfully using " +fra);	
			}
		} catch (NumberFormatException e) {
			//e.printStackTrace();
			reportSteps("fail", "Number Format exception");
		}
		takeSnap();
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			//System.out.println("Alert Accepted");
			reportSteps("pass", "Alert Accepted");
		} catch (NoAlertPresentException e) {
			reportSteps("fail", "Alert not present");
			//System.out.println("Alert not present");
		}
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportSteps("pass", "Alert dismissed");
			//System.out.println("Alert dismissed");
		} catch (NoAlertPresentException e) {
			reportSteps("fail", "Alert not present");
			//System.out.println("Alert not present");
		}
		takeSnap();
	}

	@Override
	public String getAlertText() {
		String text="";
		try {
			text = driver.switchTo().alert().getText();
			reportSteps("pass", "Text fetched from Alert");
		} catch (WebDriverException e) {
			//e.printStackTrace();
			reportSteps("pass", "Failed to fetch the Text from Alert");
		}
		return text;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snap/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
		
	}
	
	@Override
	public void closeBrowser() {
		try {
			driver.close();
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
		} catch (WebDriverException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		
	}


}
