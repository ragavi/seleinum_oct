package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods{
	
	@DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		return ReadExcel.getExcelData(excelFileName);
	}
	
	
	
	@AfterMethod(groups="config")
	public void ExitBrowser() {
		closeBrowser();
	}
	@Parameters({"browser","url","uname","pass"})
	@BeforeMethod(groups="config")
	public void login(String browser,String url,String uname,String pass) {
		startApp(browser, url);
		//startApp("chrome", "http://leaftaps.com/opentaps/");
		WebElement eleUsername = locateElement("id", "username");
		//type(eleUsername, "DemoSalesManager");
		type(eleUsername,uname);
		WebElement elePassword = locateElement("id", "password");
	//	type(elePassword, "crmsfa");
		type(elePassword, pass);
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");		
		click(eleLogin); 
	}

}
