package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;


public class TC001_CreateLead extends ProjectMethods{
	
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC001_CL";
		testDesc ="Create a new lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	//@Test(priority=5)
	//@Test(invocationCount=2,invocationTimeOut=20000)
	@Test(groups="test.smoke")
	public void createLead() {
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//Click Create Leads
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		//Enter Company Name
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, "VolanteSoftware");
		//Enter FirstName
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, "Ragavi");
		//LastName
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "Mani");
		//Dropdown Select Sorce
		WebElement eleSource = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Employee","visible");
		//Select Marketing Company
		WebElement eleMarket = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleMarket, "Catalog Generating Marketing Campaigns","visible");
		//FirstName Local
		WebElement eleFirstNameLocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFirstNameLocal, "RagaviMani");
		//LastName Local
		WebElement eleLastNameLocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLastNameLocal, "Mani");
		//Salutation
		type(locateElement("id", "createLeadForm_personalTitle"), "Tester");
		//Title
		type(locateElement("id","createLeadForm_generalProfTitle"),"Test");
		//Dept
		type(locateElement("id","createLeadForm_departmentName"), "Msg");
		//Revenue
		type(locateElement("id","createLeadForm_annualRevenue"),"323242");
		//Preferred 	Currency
		WebElement eleCurr = locateElement("id","createLeadForm_currencyUomId");
		selectDropDownUsingText(eleCurr, "AFA - Afghani","visible");
		//Select Industry
		WebElement eleIndustry = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingText(eleIndustry, "Finance","visible");
		//No of Employees
		type(locateElement("id", "createLeadForm_numberEmployees"), "100");
		//Select Ownership
		WebElement eleOwner = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleOwner, "Sole Proprietorship","visible");
		//sic code
		type(locateElement("id","createLeadForm_sicCode"),"SIC_12345");
		//Ticker Symbol
		type(locateElement("id","createLeadForm_tickerSymbol"), "@");
		//Description
		type(locateElement("id","createLeadForm_description"), "Description of the lead to maintain the team");
		//Imp Note
		type(locateElement("id","createLeadForm_importantNote"), "Important note");
		//Contact Details
		//Country Code
		type(locateElement("id","createLeadForm_primaryPhoneCountryCode"),"3");
		//AreaCode
		type(locateElement("id","createLeadForm_primaryPhoneAreaCode"),"+91");
		//Phone Extension
		type(locateElement("id","createLeadForm_primaryPhoneExtension"),"270");
		//Email
		type(locateElement("id","createLeadForm_primaryEmail"),"nan@gmail.com");
		//PhoneNo
		type(locateElement("id","createLeadForm_primaryPhoneNumber"),"9854356782");
		//Person to Ask for
		type(locateElement("id","createLeadForm_primaryPhoneAskForName"),"Employee");
		//Web URL
		type(locateElement("id","createLeadForm_primaryWebUrl"),"www.volantetech.org");
		// Primary Address Pane
		//Name
		//Calender
		type(locateElement("id","createLeadForm_generalToName"),"ToName");
		//Address1
		type(locateElement("id","createLeadForm_generalAddress1"),"Ram Nagar");
		//City
		type(locateElement("id","createLeadForm_generalCity"),"Chennai");
		//Postal Code
		type(locateElement("id","createLeadForm_generalPostalCode"),"600100");
		//Postal Extension 
		type(locateElement("id","createLeadForm_generalPostalCodeExt"),"123");
		//Attention Name
		type(locateElement("id","createLeadForm_generalAttnName"),"AttentionName");
		//Address2
		type(locateElement("id","createLeadForm_generalAddress2"),"Pallikarani");
		//Choose State
		WebElement Statedrop=locateElement("id","createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(Statedrop, "New York", "visible");
		//Country			
		WebElement Ctrydrop=locateElement("id","createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(Ctrydrop, "USA", "value");
		//Click Lead button 	
		WebElement eleClick = locateElement("class", "smallSubmit");
		click(eleClick);
		//Verify Firstname
		WebElement verifyFirstName = locateElement("id","viewLead_firstName_sp");
		verifyExactText(verifyFirstName, "Ragavi");
	}

}









