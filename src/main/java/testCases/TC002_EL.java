package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC002_EL extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC002_EditLead";
		testDesc ="Edit a lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	
	@Test(dataProvider="fetchData")
	public void editlead(String fname,String cname) throws InterruptedException {
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Find Leads
		locateElement("linktext","Find Leads").click();
		WebElement eleFirstName=locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName,fname);
		//Click Find Lead button
		WebElement clkLead=locateElement("xpath","//button[text()='Find Leads']");
		click(clkLead);
		//First lead
		WebElement clkFirstLead=locateElement("linktext", fname);
		click(clkFirstLead);
		//Verify Title --> Need to be check
		/*WebElement ExpectedTitle = locateElement("xpath", "//title[text()='View Lead | opentaps CRM')]");
		String a=getText(ExpectedTitle);
		verifyTitle(a);*/
		verifyTitle("View Lead | opentaps CRM");
		//click Edit
		WebElement clkEdit=locateElement("linktext", "Edit");
		click(clkEdit);
		//Change the company name
		type(locateElement("id", "updateLeadForm_companyName"),cname);
		//Click Update
		WebElement clkUpdate=locateElement("class", "smallSubmit");
		click(clkUpdate);
		//confirm changed name appears
		WebElement verifyCompanyName = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(verifyCompanyName, "TCS");
		
			
		}
	@DataProvider(name="fetchData")
	public String[][] getData() {
		String[][] data=new String[2][2];
		data[0][0]="Ragavi";
		data[0][1]="TCS";
		
		data[1][0]="Suvedha";
		data[1][1]="Wipro";
		
		return data;
	}

}
