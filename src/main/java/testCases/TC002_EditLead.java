package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC002_EditLead";
		testDesc ="Edit a lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	//@Test(priority=4)
	@Test(groups="test.sanity", dependsOnGroups="test.smoke")
	public void editlead() throws InterruptedException {
	//	login();
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Find Leads
		locateElement("linktext","Find Leads").click();
		WebElement eleFirstName=locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName,"Ragavi");
		//Click Find Lead button
		WebElement clkLead=locateElement("xpath","//button[text()='Find Leads']");
		click(clkLead);
		//First lead
		WebElement clkFirstLead=locateElement("linktext", "Ragavi");
		click(clkFirstLead);
		//Verify Title --> Need to be check
		/*WebElement ExpectedTitle = locateElement("xpath", "//title[text()='View Lead | opentaps CRM')]");
		String a=getText(ExpectedTitle);
		verifyTitle(a);*/
		verifyTitle("View Lead | opentaps CRM");
		//click Edit
		WebElement clkEdit=locateElement("linktext", "Edit");
		click(clkEdit);
		//Change the company name
		type(locateElement("id", "updateLeadForm_companyName"),"TCS");
		//Click Update
		WebElement clkUpdate=locateElement("class", "smallSubmit");
		click(clkUpdate);
		//confirm changed name appears
		WebElement verifyCompanyName = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(verifyCompanyName, "TCS");
		//close browser
		//closeBrowser();
		
	}

}
