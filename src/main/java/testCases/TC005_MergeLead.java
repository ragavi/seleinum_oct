package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC005_MergeL";
		testDesc ="Merge lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	@Test(groups="test.smoke")
	public void mergeLead() throws InterruptedException {
		
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Click Merge leads
		WebElement mergeLeads=locateElement("linktext", "Merge Leads");
		click(mergeLeads);
		//Click on Icon near From Lead
		WebElement FromLeadIcon=locateElement("xpath", "//img[@alt='Lookup']");
		click(FromLeadIcon);
		//Move to new window
		switchToWindow(1);
		//Enter Lead ID
		type(locateElement("name", "id"),"10082");
		//Click Find Leads button
		WebElement clkFindLead=locateElement("xpath", "//button[text()='Find Leads']");
		click(clkFindLead);
		Thread.sleep(3000);
		//Click First Resulting lead
		WebElement clkFirstLead=locateElement("linktext", "Ragavi");
		click(clkFirstLead);
		//Switch to primary window
		/*String mainWindowHandle = driver.getWindowHandles().iterator().next();
		driver.switchTo().window(mainWindowHandle);*/
		ParentWindow();
		System.out.println(driver.getTitle());
		//Click on Icon near To Lead
		WebElement ToLeadIcon=locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(ToLeadIcon);		
		//Move to new window
		switchToWindow(1);
		System.out.println(driver.getTitle());
		//Enter Lead ID
		type(locateElement("name", "id"),"10114");
		//Click Find Leads button
		WebElement clkFindLead1=locateElement("xpath", "//button[text()='Find Leads']");
		click(clkFindLead1);
		Thread.sleep(3000);
		//Click First Resulting lead
		WebElement clkFirstLead1=locateElement("linktext", "Nandi");
		click(clkFirstLead1);
		//Switch back to primary window
		/*String mainWindowHandle1 = driver.getWindowHandles().iterator().next();
		driver.switchTo().window(mainWindowHandle1);*/
		ParentWindow();
		System.out.println(driver.getTitle());
		//Click Merge
		WebElement clkMerge=locateElement("linktext", "Merge");
		click(clkMerge);
		//Switch to alert then accept
		acceptAlert();
		//Click Find Lead
		WebElement clkLead=locateElement("linktext", "Find Leads");
		click(clkLead);
		//Enter From Lead ID
		type(locateElement("name", "id"),"10082");
		WebDriverWait wait=new WebDriverWait(driver, 40);
		WebElement findLead = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Find Leads']")));
		click(findLead);	
		//Error Msg	
		String errMsg=getText(locateElement("xpath", "//div[text()='No records to display']"));
		String ExpectedMsg="No records to display";
		if(errMsg.equals(ExpectedMsg)) {
			System.out.println("Testcase Passed: No records to display");
		}else {
			System.out.println("Testcase Failed");
		}		
		String text = getText(locateElement("xpath", "//div[@class='x-toolbar x-small-editor']/div[@class='x-paging-info']"));
    	System.out.println("Result is :"+text);
	}
}
