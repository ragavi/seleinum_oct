package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC004_DupLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC004_DupLead";
		testDesc ="Create a new lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	//@Test(priority=1)
	@Test(groups="test.sanity")
	public void Duplead() throws InterruptedException {
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Find Leads
		locateElement("linktext","Find Leads").click();
		//Click on Email
		locateElement("xpath","//span[text()='Email']").click();
		//Enter Email
		type(locateElement("name", "emailAddress"),"nandhurags@gmail.com");
		//Click find leads button
		WebElement clkLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(clkLead);
		//Capture name of First Resulting lead
		WebElement name=locateElement("linktext","Ragavi");
	//	String firstname=getText(name);
		//Click First Resulting lead
		click(name);
		//Click Duplicate Lead
		WebElement clkDuplicateLead=locateElement("linktext","Duplicate Lead");
		click(clkDuplicateLead);
		//Verify Title 
		//String title=getText(locateElement("xpath", "//title[contains(text(),'Duplicate Lead')]"));
		//System.out.println("Title" +title);
		//verifyTitle("Duplicate Lead");
		//System.out.println(driver.getTitle());
		//Click Create Lead
		WebElement clkCreateLead=locateElement("class","smallSubmit");
		click(clkCreateLead);
		//Confirm the duplicated lead name is same as captured name
		WebElement confirmName=locateElement("id", "viewLead_firstName_sp");
		verifyExactText(confirmName, "Ragavi");
		//close browser
		//closeBrowser();
		
	}
}
