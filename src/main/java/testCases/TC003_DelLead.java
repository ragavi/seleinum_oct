package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC003_DelLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC003_DelLead";
		testDesc ="Delete a existing lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	//@Test(priority=3)
	@Test(dataProvider="fetchData")
	public void delLead(String phCtrycode,String phArea,String phNo,String capid) throws InterruptedException {
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Find Leads
		locateElement("linktext","Find Leads").click();
		//Click on Phone
		locateElement("xpath","//span[text()='Phone']").click();
		//Enter phone number
		type(locateElement("name", "phoneCountryCode"),phCtrycode);
		type(locateElement("name", "phoneAreaCode"),phArea);
		type(locateElement("name", "phoneNumber"),phNo);
		//Click find leads button
		WebElement clkLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(clkLead);
		//Capture Lead Id of first 
		Thread.sleep(4000);
		WebElement LeadID = locateElement("linktext", capid);
		String leadid=getText(LeadID);
		System.out.println(leadid);
		//Click first resulting lead
		WebElement firstLead=locateElement("linktext", "Ragavi");
		click(firstLead);
		//Click Delete
	WebElement delLead=locateElement("linktext", "Delete");
		click(delLead);
		//Find Leads
		WebElement findleads = locateElement("linktext","Find Leads");
		click(findleads);
		//Enter captured id
		type(locateElement("xpath", "//input[@class=' x-form-text x-form-field ']"),capid);
		//click find leads button
		WebElement findbtn = locateElement("xpath","//button[text()='Find Leads']");
		click(findbtn);
		//Verify Error Message
		WebElement errMsg=locateElement("xpath","//div[text()='No records to display']");
		getText(errMsg);
		//Close the browser (Do not log out)
		//closeBrowser();
		}
	@DataProvider(name="fetchData")
	public String[][] getData(){
		String[][] data=new String[2][4];
		data[0][0]="3";
		data[0][1]="91";
		data[0][2]="9854356782";
		data[0][3]="10628";
		
		data[0][0]="3";
		data[0][1]="91";
		data[0][2]="9854356781";
		data[0][3]="10683";
		return data;
		
	}
}
