package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods{
	@BeforeClass(groups="config")
	public void setData() {
		testcaseName ="TC003_DelLead";
		testDesc ="Delete a existing lead in leaftaps";
		author ="Ragavi";
		category = "Smoke";
	}
	//@Test(priority=3)
	@Test(groups="test.sanity", dependsOnGroups="test.smoke")
	public void delLead() throws InterruptedException {
		//Click CRM/SFR Link
		WebElement eleCRMLink = locateElement("linktext", "CRM/SFA");
		click(eleCRMLink); 
		//To click create lead button
		locateElement("xpath","//a[text()='Leads']").click();
		//Find Leads
		locateElement("linktext","Find Leads").click();
		//Click on Phone
		locateElement("xpath","//span[text()='Phone']").click();
		//Enter phone number
		type(locateElement("name", "phoneCountryCode"),"3");
		type(locateElement("name", "phoneAreaCode"),"91");
		type(locateElement("name", "phoneNumber"),"9854356782");
		//Click find leads button
		WebElement clkLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(clkLead);
		//Capture Lead Id of first 
		Thread.sleep(4000);
		WebElement LeadID = locateElement("linktext", "10943");
		String leadid=getText(LeadID);
		System.out.println(leadid);
		//Click first resulting lead
		WebElement firstLead=locateElement("linktext", "Ragavi");
		click(firstLead);
		//Click Delete
	WebElement delLead=locateElement("linktext", "Delete");
		click(delLead);
		//Find Leads
		WebElement findleads = locateElement("linktext","Find Leads");
		click(findleads);
		//Enter captured id
		type(locateElement("xpath", "//input[@class=' x-form-text x-form-field ']"),"10943");
		//click find leads button
		WebElement findbtn = locateElement("xpath","//button[text()='Find Leads']");
		click(findbtn);
		//Verify Error Message
		WebElement errMsg=locateElement("xpath","//div[text()='No records to display']");
		getText(errMsg);
		//Close the browser (Do not log out)
		//closeBrowser();
		}
}
