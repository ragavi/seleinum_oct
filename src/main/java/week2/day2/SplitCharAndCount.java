package week2.day2;

import java.util.Scanner;

public class SplitCharAndCount {

	public static void main(String[] args) {
		// Input a String, split the char and increment the count if the char repeats.
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String:");
		String input=sc.nextLine();
		  char[] ch = input.toCharArray();
		  
		  	for(int i=0;i<ch.length;i++) {
		  		 int count=0;
		  		for(int j=0;j<ch.length;j++)
		  		{
		  			if(ch[i]==ch[j])
		  			{ 
		  				count++;
		  			}
		  		}
		  
					System.out.println("CHARACTERS : "+ch[i]+" "+count);
				}
		  	
			
		}
	}


