package week2.day2;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapDupCount {

	public static void main(String[] args) {
		//  Input a String, split the char and increment the count if the char repeats.
		String name="testleaf";		
		char[] ch=name.toCharArray();
		Map<String, Integer> map=new LinkedHashMap<>();
		for (char c : ch) {
			int count=0;
			for(int i=0;i<ch.length;i++) {
				if(c==ch[i]) {
					count++;
				}
			}
			map.put(String.valueOf(c),count);
		}
		System.out.println(map);
	}

}
