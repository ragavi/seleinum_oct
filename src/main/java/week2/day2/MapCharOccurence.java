package week2.day2;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapCharOccurence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="Testleaf";
		char[] ch=str.toCharArray();
		Map<Character, Integer> occ=new LinkedHashMap<>();
		for (char c : ch) {
			if(occ.containsKey(c)) {
				occ.put(c, occ.get(c)+1);
			}else {
				occ.put(c, 1);
			}
		}
		System.out.println(occ);
	}
}
