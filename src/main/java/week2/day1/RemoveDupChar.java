package week2.day1;

import java.util.Scanner;

public class RemoveDupChar {

	public static void main(String[] args) {
		// Remove duplicate char from the string

		String s="goodday";
		char [] temp = s.toCharArray();
	    int length =temp.length; 
	    for (int i=0;i<length;i++)
	    {
	        for (int j = i+1; j<length;j++)
	        {
	            if(temp[i]==temp[j])
	            {
	                int test =j;
	                for(int k=j+1; k<length ; k++)
	                {
	                    temp[test] = temp[k];
	                    test++;
	                }
	                length--;
	                j--;
	            }
	        }
	    }
	    System.out.println(String.copyValueOf(temp).substring(0,length));
	}

}
