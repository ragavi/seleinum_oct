package week2.day1;

import java.util.Scanner;

public class IndexCharCase {

	public static void main(String[] args) {
		// Input String and change all the odd index in UPPERCASE and even index in lowercase.
		
		Scanner scan = new Scanner(System.in); 
		System.out.println("Enter name:");
		String name = scan.nextLine();
		for (int i=0; i<name.length(); i++) {
		    char ch = name.charAt(i);
		    if (i % 2 == 0) {
		        System.out.print(Character.toLowerCase(ch));
		    } else {
		        System.out.print(Character.toUpperCase(ch));
		    }
		}
		System.out.println();
		scan.close();
	}

}
