package week2.day1;

import java.util.ArrayList;
import java.util.List;

public class ListMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> ls=new ArrayList<String>();
		ls.add("Mouli");
		ls.add("Kani");
		ls.add("Ragavi");
		ls.add("Priya");
		ls.add("Mouli");
		System.out.println(ls.remove("Mouli"));		
		System.out.println(ls.contains("Ragavi"));
		for(int i=0;i<ls.size();i++) {
			System.out.println(ls.get(i));
		}
		System.out.println(ls.get(3));
		ls.clear();
		System.out.println(ls.isEmpty());
		System.out.println(ls.size());
	}

}
