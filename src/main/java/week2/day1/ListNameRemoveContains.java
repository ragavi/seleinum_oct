package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListNameRemoveContains {

	public static void main(String[] args) {
		// List the names and remove the name which contains d,print after sorting
		List<String> name=new ArrayList<>();
		name.add("Dhana");
		name.add("Abi");
		name.add("priyadharshini");
		name.add("sudha");
		name.add("Ragavi");
		name.add("Suvedha");
		name.add("Poornima devi");
		name.add("Nandhini");
		System.out.println("Names present in the List are:");
		for (String Names : name) {
			System.out.println(Names);			
		}
		for(int i=0;i<name.size();) {
			if(name.get(i).contains("d")) {
				name.remove(i);
			}else {
				i++;
			}
		}
		System.out.println("Names present in the List after removing contains 'd':");
		Collections.sort(name);
		System.out.println(name);
	}

}
