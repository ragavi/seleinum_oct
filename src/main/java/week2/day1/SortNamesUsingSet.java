package week2.day1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SortNamesUsingSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// List empid with dup and print in sorting order
		List<String> empId=new ArrayList<>();
		empId.add("Vs140");
		empId.add("Vs142");
		empId.add("Vs143");
		empId.add("Vs141");
		empId.add("Vs141");
		empId.add("Vs142");
		empId.add("Vs144");
		//Remove Duplicates
		/*	Set<String> set=new LinkedHashSet<>();
			System.out.println("Employee ID after removing duplicated:");
			set.addAll(empId);
			for (String EmployeeID : set) {
				System.out.println(EmployeeID);
			}*/
			//Remove Duplicates and Print Ascending order
			Set<String> tree=new TreeSet<>();
			System.out.println("Ascending order of Employee ID after removing duplicated:");
			tree.addAll(empId);
			for (String ID : tree) {
				System.out.println(ID);
			}
	}

}
