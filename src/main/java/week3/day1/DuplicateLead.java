package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// Create object for ChromeDriver class
		ChromeDriver driver=new ChromeDriver();
		//To maximize the browser window
		driver.manage().window().maximize();
		//To load the Url in browser
		driver.get("http://leaftaps.com/opentaps");
		//Enter the username
		WebElement eleUserName=driver.findElementByXPath("//input[@id='username']");
		//sendKeys method used to set the value
		eleUserName.sendKeys("DemoSalesManager");
		//Enter password , another way to set the values
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		//click button
		WebElement eleLogin=driver.findElementByXPath("//input[@class='decorativeSubmit']");
		eleLogin.click();
		//To click the anchor-><a href> Text
		WebElement CrmLink=driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]");
		CrmLink.click(); 
		//To click create lead button
		driver.findElementByXPath("//a[text()='Leads']").click();
		//Find Leads
		WebElement find = driver.findElementByXPath("//a[text()='Find Leads']");
		find.click();
		//Click on Phone
		driver.findElementByXPath("//span[text()='Phone']").click();
		//Enter phone number
		WebElement CtryCode = driver.findElementByName("phoneCountryCode");
		CtryCode.clear();
		CtryCode.sendKeys("1");
		driver.findElementByName("phoneAreaCode").sendKeys("91");
		driver.findElementByName("phoneNumber").sendKeys("9843428391");
		//Click find leads button
		WebElement clickFindLeadButton = driver.findElementByXPath("//button[text()='Find Leads']");
		clickFindLeadButton.click();
		//Capture Lead Id of first 
		Thread.sleep(4000);
		WebElement LeadID = driver.findElementByXPath("(//a[text()='12921'])");
		System.out.println("Lead ID:");
		String leadid=LeadID.getText();
		System.out.println();
		//Click First Resulting lead		
		driver.findElementByXPath("(//a[text()='Ragavi'])").click();
		//Click Delete
		driver.findElementByXPath("//a[text()='Delete']").click();
		//Click Findleads
		//find.click(); -->Doubt
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		//Enter captured lead ID
		driver.findElementByXPath("//input[@class=' x-form-text x-form-field ']").sendKeys(leadid);
		//Click find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//Verify Error Message
		WebElement errMsg=driver.findElementByXPath("//div[text()='No records to display']");
		System.out.println("Serach found messgae: "+errMsg.getText());
		//Close the browser (Do not log out)
		driver.close();
	}
}
