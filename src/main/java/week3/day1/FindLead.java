package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindLead {

	public static void main(String[] args) throws InterruptedException {
				//Set Property for ChromeDriver
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				// Create object for ChromeDriver class
				ChromeDriver driver=new ChromeDriver();
				//To maximize the browser window
				driver.manage().window().maximize();
				//To load the Url in browser
				driver.get("http://leaftaps.com/opentaps");
				//Enter the username
				WebElement eleUserName=driver.findElementByXPath("//input[@id='username']");
				//sendKeys method used to set the value
				eleUserName.sendKeys("DemoSalesManager");
				//Enter password , another way to set the values
				driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
				//click button
				WebElement eleLogin=driver.findElementByXPath("//input[@class='decorativeSubmit']");
				eleLogin.click();
				//To click the anchor-><a href> Text
				WebElement CrmLink=driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]");
				CrmLink.click(); 
				//driver.findElementByLinkText("CRM/SFA").click();
				//To click create lead button
				driver.findElementByXPath("//a[text()='Leads']").click();
				//Find Leads
				WebElement find = driver.findElementByXPath("//a[text()='Find Leads']");
				find.click();
				//Enter name
				driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Ragavi");
				//Click Button
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				Thread.sleep(4000);
				driver.findElementByXPath("(//a[text()='Ragavi'])[3]").click();
			
				driver.findElementByXPath("//a[text()='Edit']").click();
				//Update the LastName, first need to clear the available text then enter new value
				WebElement update = driver.findElementByXPath("//input[@id='updateLeadForm_lastName']");
				update.clear();
				update.sendKeys("Mani");
				driver.findElementByXPath("//input[@id='updateLeadForm_generalProfTitle']").sendKeys("Tester");
				//Update button
				driver.findElementByXPath("//input[@value='Update']").click();
	}

}
