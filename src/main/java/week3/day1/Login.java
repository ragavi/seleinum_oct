package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;

//import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		//Set Property for ChromeDriver
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// Create object for ChromeDriver class
		ChromeDriver driver=new ChromeDriver();
		//To maximize the browser window
		driver.manage().window().maximize();
		//To load the Url in browser
		driver.get("http://leaftaps.com/opentaps");
		JavascriptExecutor js = (JavascriptExecutor) driver;  
		String text = js.executeScript("return document.getElementById('appName').innerHTML").toString();
		System.out.println(text);
		System.out.println("*******************************************");
	//	Object x = js.executeScript("document.getElementById('appName').textContent");
	//	System.out.println(x);
		//Enter the username
		WebElement eleUserName=driver.findElementById("username");
		//sendKeys method used to set the value
		eleUserName.sendKeys("DemoSalesManager");
		//Enter password , another way to set the values
		driver.findElementById("password").sendKeys("crmsfa");
		//click button
		driver.findElementByClassName("decorativeSubmit").click();
		//To click the anchor-><a href> Text
		driver.findElementByLinkText("CRM/SFA").click();
		//To click create lead button
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ragavi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Software");
		Object txt = driver.executeScript("document.getElementsByClassName('tableheadtext')");
		
		System.out.println(txt);
		//Title of the page using js
	
		String sText =  js.executeScript("return document.title;").toString();
		System.out.println(sText);
		
		
	/*	JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("")
		
		
		
		//select value from dropdown list
		/*		WebElement srcDrop=driver.findElementById("createLeadForm_dataSourceId");
		Select select1=new Select(srcDrop);
		select1.selectByVisibleText("Employee");
		//TextBox
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Volante");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Tester");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Test Engineer");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("12000");
		//Dropdown2
		WebElement IndustryDrop=driver.findElementById("createLeadForm_industryEnumId");
		Select select2=new Select(IndustryDrop);
		select2.selectByIndex(2);
		//To print all value
		/*List<WebElement> allOption=select2.getOptions();
		for (WebElement eachOption : allOption) {
			System.out.println(eachOption.getText());			
		}*/
			
		//To print option starts with 'C'
		/*List<WebElement> allOption=select2.getOptions();
		for (WebElement eachOption : allOption) {
			if(eachOption.getText().startsWith("C"))
			{
				System.out.println(eachOption.getText());
			}
		}*/
		//Dropdown3
	/*	WebElement Ownerdrop=driver.findElementById("createLeadForm_ownershipEnumId");
		Select select3=new Select(Ownerdrop);
		select3.selectByValue("OWN_PARTNERSHIP");
		//TextBox
		driver.findElementById("createLeadForm_sicCode").sendKeys("sicVS142");
		//TextArea
		driver.findElementById("createLeadForm_description").sendKeys("Description of the lead to maintain the team");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Important Note is to noted carefully");
		driver.findElementById("createLeadForm_lastName").sendKeys("Software");
		//Dropdown4
		WebElement Marketingdrop=driver.findElementById("createLeadForm_marketingCampaignId");
		Select select4=new Select(Marketingdrop);
		select4.selectByVisibleText("Catalog Generating Marketing Campaigns");
		//Textbox
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Technologies");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Messaging");
		//Dropdown5
		WebElement Currencydrop=driver.findElementById("createLeadForm_currencyUomId");
		Select select5=new Select(Currencydrop);
		select5.selectByVisibleText("INR - Indian Rupee");
		//No of employees
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$");	
		
		//Contact Info Pane ->TextBox already defaults with '1'
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("3");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("201");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("ragavi_V142@volantetech.net");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9854356782");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Employee");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.volantetech.org");
		// Primary Address Pane
		driver.findElementById("createLeadForm_generalToName").sendKeys("ToName");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Ram Nagar");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600100");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("123");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("AttentionName");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Pallikarani");
		//No option inside Select -> Doubt
		WebElement Statedrop=driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select select6=new Select(Statedrop);
		select6.selectByVisibleText("New York");
		
		WebElement Ctrydrop=driver.findElementById("createLeadForm_generalCountryGeoId");
		Select select7=new Select(Ctrydrop);
		select7.selectByVisibleText("United States");*/
		//Click Lead button 
		//driver.findElementByName("submitButton").click();	 //why cant with name	
		driver.findElementByClassName("smallSubmit").click();
		
		
	}

}
