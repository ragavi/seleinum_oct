package week3.day1;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.chrome.ChromeDriver;

public class UncheckCheckBox {

	public static void main(String[] args) {
		/*System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.testleaf.herokuapp.com/pages/checkbox.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);*/
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Password : ");
		String value = sc.nextLine();
		String pat = "^(?=.*[0-9]{2})([a-z])(?=.*[A-Z])(?=\\S+$).{10,}$";
		Pattern pattern = Pattern.compile(pat);
		Matcher M = pattern.matcher(value);
		if (M.matches()) {

			System.out.println("Password Verification Success");
		} else {
			System.out.println("Password Verification Failed");
		}

	}

}
