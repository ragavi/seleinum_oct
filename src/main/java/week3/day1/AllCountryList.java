package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AllCountryList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement Wbctry=driver.findElementById("userRegistrationForm:countries");
		Select selectCtry=new Select(Wbctry);	
		List<WebElement> newCtry=selectCtry.getOptions();
		System.out.println("Country list from Agent SignUp in IRCTC page");
		for (WebElement ListOption : newCtry) {
			System.out.println(ListOption.getText());
		}	
	}

}
