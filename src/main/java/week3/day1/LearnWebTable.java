package week3.day1;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWebTable {

	public static void main(String[] args) throws IOException {
		//Set Property for ChromeDriver
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// Create object for ChromeDriver class
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		//To wait till thw webpage gets loaded
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement fromStation = driver.findElementById("txtStationFrom");
		fromStation.clear();
		fromStation.sendKeys("MAS",Keys.TAB);
		WebElement toStation = driver.findElementById("txtStationTo");
		toStation.clear();
		toStation.sendKeys("JTJ",Keys.TAB);
		WebElement check=driver.findElementById("chkSelectDateOnly");
		if(check.isSelected()) {
			check.click();
		}
		driver.findElementByLinkText("Train Name").click();
		//First find the table name
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		//find rows using <tr> tagname
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//size of rows
		System.out.println(rows.size());
		for(int i=0;i<rows.size();i++) {
			WebElement allRows=rows.get(i);
			//findElements used to fetch all the data which is present in the table data (cell)
			List<WebElement> allCell=allRows.findElements(By.tagName("td"));
			//size of <td> tag
			//System.out.println(allCell.size()); 
			System.out.println(allCell.get(1).getText());
		}
		File src=driver.getScreenshotAs(OutputType.FILE);
		File descLocation=new File("./snaps/erailshot.png");
		//Directly from driver to local machine selenium cant handle so use class FileUtils with IOException(ie., to avoid compile time exception)
		FileUtils.copyFile(src, descLocation);
	}

}
