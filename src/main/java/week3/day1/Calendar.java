package week3.day1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import freemarker.template.SimpleDate;

public class Calendar {

	public static void main(String[] args) throws ParseException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// Create object for ChromeDriver class
		ChromeDriver driver=new ChromeDriver();
		//To maximize the browser window
		driver.manage().window().maximize();
		//To load the Url in browser
		driver.get("http://leaftaps.com/opentaps");
		//Enter the username
		WebElement eleUserName=driver.findElementById("username");
		//sendKeys method used to set the value
		eleUserName.sendKeys("DemoSalesManager");
		//Enter password , another way to set the values
		driver.findElementById("password").sendKeys("crmsfa");
		//click button
		driver.findElementByClassName("decorativeSubmit").click();
		//To click the anchor-><a href> Text
		driver.findElementByLinkText("CRM/SFA").click();
		//To click create lead button
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ragavi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Software");
		//Calendar
		WebElement calendar =driver.findElementById("createLeadForm_birthDate-button");
		calendar.click();
		WebElement table = driver.findElementByXPath("//div[@class='calendar']/table");
				//find rows using <tr> tagname
				List<WebElement> rows = table.findElements(By.tagName("tr"));
				//size of rows
				System.out.println(rows.size());
				//Year
				driver.findElementByXPath("(//td[@class='button nav'])[1]").click();
				//Month
				driver.findElementByXPath("(//td[@class='button nav'])[2]").click();
				//day
				driver.findElementByXPath("(//td[@class='day name'])[4]").getText();
			
					
	}

}
