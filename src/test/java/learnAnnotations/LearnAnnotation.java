package learnAnnotations;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class LearnAnnotation {

  @BeforeSuite
  public void Wakeup() {
		  System.out.println("Wake up");
	  }
  @BeforeTest
  public void Brush() {
	  System.out.println("Brush");
  }
  @BeforeClass
  public void CasualDress() {
	  System.out.println("Wear casual dress");
  }
  @BeforeMethod
  	public void getIntoOffice() {
	  System.out.println("Entered office");
  }
  @Test
  public void GoToBreak() {
	  System.out.println("Tea break");
  }
  @Test
  public void GoToLunch() {
	  System.out.println("Lunch Time!!!!");
  }
  @AfterMethod
  public void getOutOfOffice() {
	  System.out.println("Out of office");
  }

  @AfterClass
  public void HomeDress() {
	  System.out.println("Wear night dress");
  }

  @AfterTest
  public void beforeBed() {
	  System.out.println("Brush before go to bed");
  }

  @AfterSuite
  public void GoAndSleep() {
	  System.out.println("Sleeep!!!!!!!");
  }

}
